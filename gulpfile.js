// --- PLUGINS ---
var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var svgSymbols = require('gulp-svg-symbols');
var image = require('gulp-image');
var livereload = require('gulp-livereload');
var fs = require('fs');
var ttf2woff = require('gulp-ttf2woff');




// --- CLEAN ---
gulp.task('clean', function() {
	gulp.src('dist')
		.pipe(clean());
});



// --- COPY ---
gulp.task('copy', function(){
	gulp.src(['node_modules/popper.js/dist/**/*'])
		.pipe(gulp.dest('assets/popper.js/'));
	gulp.src(['node_modules/jquery/dist/**/*'])
		.pipe(gulp.dest('assets/jquery/'));
});



// --- FONTS ---
gulp.task('ttf2woff', function(){
  gulp.src(['src/fonts/**/*.ttf'])
    .pipe(ttf2woff())
    .pipe(gulp.dest('assets/fonts/'));
});



// --- CSS ---
gulp.task('css', function() {
	gulp.src('src/scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(cleanCSS({
			rebase: false,
			level: 2
		}))
		.pipe(concat('style.min.css'))
		.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('assets/css/'));
});



// --- JS ---
var sourceJS = require('./src/js/main.json');
gulp.task('js', function() {
	gulp.src(sourceJS)
		//.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('assets/js/'));
});



// --- IMAGES ---
gulp.task('images', function() {
	gulp.src('src/img/**/*')
		.pipe(image())
		.pipe(gulp.dest('assets/img'));
});



// --- FAVICON ---
gulp.task('favicon', function() {
	gulp.src('src/favicon/**/*')
		.pipe(image())
		.pipe(gulp.dest('assets/favicon'));
});



// --- SPRITES ---
gulp.task('sprites', function () {
	gulp.src('src/svg/icons/*.svg')
		.pipe(svgSymbols({
			id: 'icon-%f',
			templates: [
				'default-svg',
				'default-css',
				'default-demo'
			],
			transformData: function(svg, defaultData, options) {
				width = (1/svg.height)*svg.width;
				return {
					id: defaultData.id,
					class: '.'+defaultData.id,
					width: width + 'em',
					height: '1em'
				};
			}
		}))
		.pipe(gulp.dest('assets/svg/icons/'))
		.pipe(gulp.dest('src/dist/svg/icons/'));

	gulp.src('src/svg/logo/*.svg')
		.pipe(svgSymbols({
			id: 'logo-%f',
			templates: [
				'default-svg',
				'default-css',
				'default-demo'
			],
			transformData: function(svg, defaultData, options) {
				width = (1/svg.height)*svg.width;
				return {
					id: defaultData.id,
					class: '.'+defaultData.id,
					width: width + 'em',
					height: '1em'
				};
			}
		}))
		.pipe(gulp.dest('assets/svg/logo/'))
		.pipe(gulp.dest('src/dist/svg/logo/'));
});



// --- WATCH ---
gulp.task('watch',function(){
	gulp.watch(['src/scss/**/*.scss'],['css']);
	gulp.watch(['src/js/**/*.js'],['js']);
	
	// LIVERELOAD CHROME EXTENTION
	livereload.listen();
	gulp.watch(['assets/css/*.css']).on('change',livereload.changed);
	gulp.watch(['assets/js/*.js']).on('change',livereload.changed);
	gulp.watch(['*.html']).on('change',livereload.changed);
});



// --- DEFAULT ---
gulp.task('default',['sprites'], function(){
	gulp.start('copy', 'ttf2woff', 'css', 'js', 'favicon', 'images');
});
 