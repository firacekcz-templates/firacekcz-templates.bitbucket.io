// --- ANIMATE ---
$('[data-animate=fadeInUp]').addClass('animate-hidden').viewportChecker({
	classToAdd: 'animate-visible animated fadeInUp',
	offset: 20,
	repeat: false,
});


// --- OWL CAROUSEL ---
$('.testimonial-carousel').addClass('owl-carousel').owlCarousel({
	loop: true,
	margin: 0,
	nav: true,
	items: 1,
})


// --- carousel buttons ---
$('.owl-dot').attr({'aria-label':'Next slide'});